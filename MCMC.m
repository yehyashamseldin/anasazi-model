
clc
clear

tic
setenv('LD_LIBRARY_PATH','//home/samuel/sfw/Boost/Boost_1.61/lib/://home/samuel/sfw/repast_hpc-2.3.0/lib/');

%setenv('LD_LIBRARY_PATH','//home/uos/sfw/Boost/Boost_1.61/lib/://home/uos/sfw/repast_hpc-2.3.0/lib/');
command = 'mpirun -n 1 bin/main.exe props/config.props props/model.props >output.txt';

runsize = 100;

%%Parameter

YearArr = cell(runsize,1);
NumHouseArr = cell(runsize,1);

propNumber = 10;

TargetData = readtable('CalibrationTargetData.csv');

Tyear = TargetData.Var1 - 800;    
TNumOfHouseholds = TargetData.Var2;
rmse = zeros(runsize,1);
propArr = zeros(propNumber,1);
propCell = cell(runsize,1);
smallestRMSE=1000;


rmseT=zeros(runsize,1);
rmseT(1)=0.3;

%MCMC
posterior_thetas_death = zeros(runsize,1);

posterior_thetas_fert = zeros(runsize,1);

posterior_thetas_harv = zeros(runsize,1);

posterior_thetas_fiss = zeros(runsize,1);

posterior_thetas_consu = zeros(runsize,1);




death_curr = 30;
fert_curr = 15;
harv_curr = 0.6;
fiss_curr = 0.09;
consu_curr = 160;

rmseCurr=1000;



figure(1)
plot(Tyear,TNumOfHouseholds)

hold on;
for i = 1:runsize
    disp(i);
%    DeathMin =  unidrnd(5) + 15;
%     DeathMax = unidrnd(2) + 36;
% 
%     FertMin =  12;
%     FertMax = unidrnd(3)+16;
% 
%     FissMax = rand/20 +0.1;
%     FissMin = rand/20;
% 
%     harvMax = rand/5 +0.6;
%     harvMin = rand/5 +0.4;
% 
%     indivConsumpMax = unidrnd(20)+160;
%     indivConsumpMin = unidrnd(20)+140;
    
    death_sd = 4;
    %MCMC
    death = normrnd(death_curr,death_sd);
    if(death<0 || death>80)
       death = death_curr;
    end
    
    
    fert_sd = 1;
    fert = normrnd(fert_curr,fert_sd);
    if(fert<0)
       fert = fert_curr;
    end
   
    harv_sd = 0.1;
    harv = normrnd(harv_curr,harv_sd);
    if(harv<0 || harv>1)
       harv = harv_curr;
    end
   
    fission_sd = 0.0125;
    fission = normrnd(fiss_curr,fission_sd);
    if(fission<0 || fission>80)
       fission = fiss_curr;
    end
  
    indivConsump_sd = 5;
    indivConsump = normrnd(consu_curr,indivConsump_sd);
    if(indivConsump<0 || indivConsump>80)
       indivConsump = consu_curr;
    end
    
    DeathMin = death-4*(death_sd);
    DeathMax = death+4*(death_sd);
    
    FertMin = fert-4*(fert_sd);
    FertMax = fert+4*(fert_sd);
    
    harvMin = harv-4*(harv_sd);
    harvMax = harv+4*(harv_sd);
    
    FissMin = fission-4*(fission_sd);
    FissMax = fission+4*(fission_sd);
    
    indivConsumpMin = indivConsump-4*(indivConsump_sd);
    indivConsumpMax = indivConsump+4*(indivConsump_sd);
    
    propArr(1) = DeathMin;
    propArr(2) = DeathMax;
    propArr(3) = FertMin;
    propArr(4) = FertMax;
    propArr(5) = FissMin;
    propArr(6) = FissMax;
    propArr(7) = harv;
    propArr(8) = harvMax;
    propArr(9) = indivConsumpMin;
    propArr(10) = indivConsumpMax;

    %disp(propArr);
    propCell{i}= propArr;
    DeathAgeMaxMsg = strcat(' death.age.max=',num2str(DeathMax));
    DeathAgeMinMsg = strcat(' death.age.min=',num2str(DeathMin));
    FertillityAgeMaxMsg = strcat(' fertile.age.max=',num2str(FertMax));
    FertillityAgeMinMsg = strcat(' fertile.age.min=',num2str(FertMin));
    FissionMaxMsg = strcat(' fission.prob.max=',num2str(FissMax));
    FissionMinMsg = strcat(' fission.prob.min=',num2str(fission));
    HarvPropMaxMsg = strcat(' percent.harvest.efficiency.max=',num2str(harvMax));
    HarvPropMinMsg = strcat(' percent.harvest.efficiency.min=',num2str(harv));
    IndivConsMaxMsg = strcat(' individual.consumption.rate.max=',num2str(indivConsumpMax));
    IndivConsMinMsg = strcat(' individual.consumption.rate.min=',num2str(indivConsump));

    command = 'mpirun -n 1 bin/main.exe props/config.props props/model.props >output.txt';

    command = strcat(command,DeathAgeMaxMsg);
    command = strcat(command,DeathAgeMinMsg);
    command = strcat(command,FertillityAgeMaxMsg);
    command = strcat(command,FertillityAgeMinMsg);
    command = strcat(command,FissionMaxMsg);
    command = strcat(command,FissionMinMsg);
    command = strcat(command,HarvPropMaxMsg);
    command = strcat(command,HarvPropMinMsg);
    command = strcat(command,IndivConsMaxMsg);
    command = strcat(command,IndivConsMinMsg);

    system(command);

    ModelData = readtable('results.csv');
    Myear = ModelData.year;
    MNumOfHouseholds = ModelData.NumOfHouseholds;
    YearArr{i} = Myear;
    NumHouseArr{i}=MNumOfHouseholds;
   

    plot(YearArr{i},NumHouseArr{i});
    rmse(i) = sqrt(1/(550)*sum((TNumOfHouseholds - NumHouseArr{i}).^2));
    
    [posterior_thetas_death,death_curr,rmseCurr] = MetropAlgo(death_curr,death,rmse(i),rmseCurr,30,death_sd,posterior_thetas_death,i);
    [posterior_thetas_fert,fert_curr,rmseCurr] = MetropAlgo(fert_curr,fert,rmse(i),rmseCurr,15,fert_sd,posterior_thetas_fert,i);
    [posterior_thetas_harv,harv_curr,rmseCurr] = MetropAlgo(harv_curr,harv,rmse(i),rmseCurr,0.6,harv_sd,posterior_thetas_harv,i);
    [posterior_thetas_fiss,fiss_min_curr,rmseCurr] = MetropAlgo(fiss_curr,fission,rmse(i),rmseCurr,0.075,fission_sd,posterior_thetas_fiss,i);
    [posterior_thetas_consu,consu_curr,rmseCurr] = MetropAlgo(consu_curr,indivConsump,rmse(i),rmseCurr,160,indivConsump_sd,posterior_thetas_consu,i);
    

end
  hold off;

% sound(sin(1:4000));
% pause(1);
% sound(sin(1:5000));

toc
[bestRMSE,bestRMSEIndex]=min(rmse);

figure(2)

plot(Tyear,TNumOfHouseholds,'b')

hold on;

for  i =1:runsize
    plot(YearArr{i},NumHouseArr{i},'r');

end

hold off;

figure(3)

plot(Tyear,TNumOfHouseholds,'b')

hold on;
plot(YearArr{bestRMSEIndex},NumHouseArr{bestRMSEIndex},'r');
        
legend('Target','LowestRMSE');
hold off;



figure(4)
histfit(posterior_thetas_death);

figure(5)
histfit(posterior_thetas_death_max);

figure(6)
histfit(posterior_thetas_fert);

figure(7)
histfit(posterior_thetas_fert_max);

figure(8)
histfit(posterior_thetas_harv);

figure(9)
histfit(posterior_thetas_harv_max);

figure(10)
histfit(posterior_thetas_fiss);

figure(11)
histfit(posterior_thetas_fiss_max);

figure(12)
histfit(posterior_thetas_consu);

figure(13)
histfit(posterior_thetas_consu_max);


