#ifndef AGENTCLASS
#define AGENTCLASS

#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include <stdio.h>
#include "CellClass.h"
#include "Network.h"
class Agent{
	
private:
	
	repast::AgentId agentId;

	bool isSatisfied;

	 // age 30 when infertile OR 
    int currentAge;
    int fertileAge; //age 16
	int infertileAge;
	bool isFertile;
	bool fissions;
    bool isExpired; 
	int deathAge;

    int maizeQuantity;
    int maxStock;
    int maizePrediction;
    double percentHarvestEfficiency;

    int numOfIndividuals; //max num = 5
    double fissionProbability; //0.125
    int individualConsumptionRate;
	int stockNeeded;
    int maxDistanceResAndField; // max 1600m
	
	
    
    void findResidence();
    void findField();
    
	
public:
	Agent(repast::AgentId id); //for init
	~Agent();

	
	
	Cell* resLocCell;
	Cell* fieldLocCell;

	bool getFissions();

	void setInitialCellRes(Cell* cell);
	Cell* getCellRes();

	bool getIsExpired();
	bool getIsFertile(){return isFertile;}
	int getFertileAge(){return fertileAge;}
	int getDeathAge(){return deathAge;}

 	void debugTestCell(Cell* cellTest);

	void updateAgentProperties(std::vector<double> agentProps);
	void decideMoveLocation(repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* space, MapNetwork* Map);
	
	/* Required Getters */
	virtual repast::AgentId& getId() { return agentId; }
	virtual const repast::AgentId& getId() const { return agentId; }
	
	/* Getters specific to this kind of Agent */

	bool getSatisfiedStatus() { return isSatisfied; }
	
	/* Actions */
	void updateStatus(repast::SharedContext<Agent>* context, repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* space, MapNetwork* Map);
	
};

/* Serializable Agent Package */
struct AgentPackage {
	
public:
    int    id;
    int    rank;
	bool isSatisfied;

	 // age 30 when infertile OR 
    int currentAge;
    int fertileAge; //age 16
	int infertileAge;
	bool isFertile;
	bool fissions;
    bool isExpired; 
	int deathAge;

    int maizeQuantity;
    int maxStock;
    int maizePrediction;
    double percentHarvestEfficiency;

    int numOfIndividuals; //max num = 5
    double fissionProbability; //0.125
    int individualConsumptionRate;
	int stockNeeded;
    int maxDistanceResAndField; // max 1600m
	Cell* resLocCell;
	Cell* fieldLocCell;


	
    /* Constructors */
    AgentPackage(); // For serialization
    AgentPackage(int _id, int _rank, bool _isSatisfied, int _currentAge, int _fertileAge, int _infertileAge, bool _isFertile, bool _fissions, bool _isExpired, int _deathAge, int _maizeQuantity,  int _maxStock, int _maizePrediction, double _percentHarvestEfficiency, int _numOfIndividuals, int _stockNeeded, int _maxDistanceResAndField, Cell* _resLocCell, Cell* _fieldLocCell);
	
    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
		ar & isSatisfied;

		// age 30 when infertile OR 
		ar & currentAge;
		ar & fertileAge; //age 16
		ar & infertileAge;
		ar & isFertile;
		ar & fissions;
		ar & isExpired; 
		ar & deathAge;

		ar & maizeQuantity;
		ar & maxStock;
		ar & maizePrediction;
		ar & percentHarvestEfficiency;

		ar & numOfIndividuals; //max num = 5
		ar & fissionProbability; //0.125
		ar & individualConsumptionRate;
		ar & stockNeeded;
		ar & maxDistanceResAndField; // max 1600m
		ar & resLocCell;
		ar & fieldLocCell;
        
    }

};

#endif