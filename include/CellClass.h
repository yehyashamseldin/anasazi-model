/*CellClass.h*/

#ifndef CELLCLASS
#define CELLCLASS
#include <stdio.h>
//#include "repast_hpc/Point.h"

//template<typename T>
class Cell{//: public repast::Point<T>{

public:
    Cell();
    Cell(int x, int y);
   

    ~Cell();

    
    void setYield(int yield);
    void setWaterSource(int water);
    void setIsField(int farm);
    void setIsRes(int res);

    int getIsField();
    int getIsRes();
    int getWaterSource();
    int getYield();
    std::vector<int> getCoor();

private:
    int isWaterSource;
    int zone;
    int maizeYield;
    int isField;
    int isResidential;
    int xCoor;
    int yCoor;
    

};

#endif