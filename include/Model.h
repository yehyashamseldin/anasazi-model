#ifndef MODEL
#define MODEL

#include <boost/mpi.hpp>
#include "repast_hpc/Schedule.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/SharedContext.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/GridComponents.h"
#include "repast_hpc/Point.h"
#include "AgentClass.h" 
#include "Network.h"
#include "CellClass.h"

class AnasaziModel{
private:

	int countOfAgents;
	int boardWidth;
    int boardHeight;
    int currentYear;
    int endYear;
	int stopAt;
	int AgentCountId;
	bool testing;
	std::vector<int>* numOfAgentsEachYear;
	
	repast::Properties* props;
	repast::SharedContext<Agent> context;
	repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* discreteSpace;
	
    void outputDataFile();
    void updateNetworkConditions();
    void doPerTick();
	void printToScreen();
	void recordResults();
	void newAgent();
	void UnitTests();

public:
	MapNetwork* map;
    void initSchedule(repast::ScheduleRunner& runner);
    void initAgents();
	
	Cell* getCell(int x, int y);
	
	AnasaziModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	~AnasaziModel();

};

#endif
