/*Network.h*/

#ifndef NETWORK
#define NETWORK

#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"

#include <boost/mpi/communicator.hpp>
#include <string>
#include "CellClass.h"
#include <vector>
#include <stdio.h>


class MapNetwork{

public:

    //getLocation(Agent *a)
    //getAgent(Cell)
    MapNetwork();
 
    ~MapNetwork();
    std::vector<std::vector<Cell*>> cells;
    int getCellZoneType(int x ,int y);
    void setCellClimateData(int year);

private:
    void readClimateData();
    void readMapFile();
    void readPDSIFile();
    void readWaterFiles();
    void initializeCells();
    
    
    int cellYieldWater[550][80][120][2]; //[Year], [x],[y], [Yield isWater] 
    int zoneAreaArray[80][120];//width and height of network -> Empty,Natural,Kinbiko,Uplands,North,Mid,Dunes,General
    
    int PDSIZoneArray[550][7];
    int waterTypeArray[80][120][3];
    int hydroArray[550][7];//"year","general","north","mid","natural","upland","kinbiko"
};

#endif