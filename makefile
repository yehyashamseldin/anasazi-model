include ./env

.PHONY: create_folders
create_folders:
	mkdir -p objects
	mkdir -p bin
	mkdir -p outputs


.PHONY: clean_compiled_files
clean_compiled_files:
	rm -f ./objects/*.o
	rm -f ./bin/*.exe
	rm -f ./outputs/*.csv
	
.PHONY: clean
clean:
	rm -rf objects
	rm -rf bin
	rm -rf outputs


.PHONY: compile
compile: clean_compiled_files
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -I./include -c ./src/Main.cpp -o ./objects/Main.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -I./include -c ./src/Model.cpp -o ./objects/Model.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -I./include -c ./src/AgentClass.cpp -o ./objects/AgentClass.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -I./include -c ./src/Network.cpp -o ./objects/Network.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -I./include -c ./src/CellClass.cpp -o ./objects/CellClass.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -I./include -c ./src/UnitTests.cpp -o ./objects/UnitTests.o
	$(MPICXX) $(BOOST_LIB_DIR) $(REPAST_HPC_LIB_DIR) -o ./bin/test.exe ./objects/UnitTests.o $(REPAST_HPC_LIB) $(BOOST_LIBS)

	$(MPICXX) $(BOOST_LIB_DIR) $(REPAST_HPC_LIB_DIR) -o ./bin/main.exe  ./objects/Main.o ./objects/Model.o ./objects/AgentClass.o ./objects/Network.o ./objects/CellClass.o $(REPAST_HPC_LIB) $(BOOST_LIBS)

.PHONY: all
all: clean create_folders compile