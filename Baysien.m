clc
clear

tic
setenv('LD_LIBRARY_PATH','//home/samuel/sfw/Boost/Boost_1.61/lib/://home/samuel/sfw/repast_hpc-2.3.0/lib/');

%setenv('LD_LIBRARY_PATH','//home/uos/sfw/Boost/Boost_1.61/lib/://home/uos/sfw/repast_hpc-2.3.0/lib/');
command = 'mpirun -n 1 bin/main.exe props/config.props props/model.props >output.txt';

runsize = 1000;

%%Parameter

YearArr = cell(runsize,1);
NumHouseArr = cell(runsize,1);

propNumber = 10;

TargetData = readtable('CalibrationTargetData.csv');

Tyear = TargetData.Var1 - 800;    
TNumOfHouseholds = TargetData.Var2;
rmse = zeros(runsize,1);
propArr = zeros(propNumber,1);
propCell = cell(runsize,1);
smallestRMSE=1000;


rmseT=zeros(runsize,1);
rmseT(1)=0.3;


figure(1)


hold on;
for i = 1:runsize
    disp(i);
    DeathMin =  unidrnd(5) + 15;
    DeathMax = unidrnd(2) + 36;

    FertMin =  12;
    FertMax = unidrnd(3)+16;

    FissMax = rand/20 +0.1;
    FissMin = rand/20;

    harvMax = rand/5 +0.6;
    harvMin = rand/5 +0.4;

    indivConsumpMax = unidrnd(20)+160;
    indivConsumpMin = unidrnd(20)+140;

    propArr(1) = DeathMin;
    propArr(2) = DeathMax;
    propArr(3) = FertMin;
    propArr(4) = FertMax;
    propArr(5) = FissMin;
    propArr(6) = FissMax;
    propArr(7) = harvMin;
    propArr(8) = harvMax;
    propArr(9) = indivConsumpMin;
    propArr(10) = indivConsumpMax;

    %disp(propArr);
    propCell{i}= propArr;
    DeathAgeMaxMsg = strcat(' death.age.max=',num2str(DeathMax));
    DeathAgeMinMsg = strcat(' death.age.min=',num2str(DeathMin));
    FertillityAgeMaxMsg = strcat(' fertile.age.max=',num2str(FertMax));
    FertillityAgeMinMsg = strcat(' fertile.age.min=',num2str(FertMin));
    FissionMaxMsg = strcat(' fission.prob.max=',num2str(FissMax));
    FissionMinMsg = strcat(' fission.prob.min=',num2str(FissMin));
    HarvPropMaxMsg = strcat(' percent.harvest.efficiency.max=',num2str(harvMax));
    HarvPropMinMsg = strcat(' percent.harvest.efficiency.min=',num2str(harvMin));
    IndivConsMaxMsg = strcat(' individual.consumption.rate.max=',num2str(indivConsumpMax));
    IndivConsMinMsg = strcat(' individual.consumption.rate.min=',num2str(indivConsumpMin));

    command = 'mpirun -n 1 bin/main.exe props/config.props props/model.props >output.txt';

    command = strcat(command,DeathAgeMaxMsg);
    command = strcat(command,DeathAgeMinMsg);
    command = strcat(command,FertillityAgeMaxMsg);
    command = strcat(command,FertillityAgeMinMsg);
    command = strcat(command,FissionMaxMsg);
    command = strcat(command,FissionMinMsg);


    command = strcat(command,HarvPropMaxMsg);
    command = strcat(command,HarvPropMinMsg);
    command = strcat(command,IndivConsMaxMsg);
    command = strcat(command,IndivConsMinMsg);

    system(command);

    ModelData = readtable('results.csv');
    Myear = ModelData.year;
    MNumOfHouseholds = ModelData.NumOfHouseholds;
    YearArr{i} = Myear;
    NumHouseArr{i}=MNumOfHouseholds;
    if(NumHouseArr{i} >-1)
        plot(YearArr{i},NumHouseArr{i},'r');
        rmse(i) = sqrt(1/(550)*sum((TNumOfHouseholds - NumHouseArr{i}).^2));
    else
        disp('Reject')
        rmse(i) = NaN;
    end
    
    
    


end

plot(Tyear,TNumOfHouseholds,'b')
hold off;

% sound(sin(1:4000));
% pause(1);
% sound(sin(1:5000));

toc
[bestRMSE,bestRMSEIndex]=min(rmse);

figure(2)




hold off;

figure(3)

plot(Tyear,TNumOfHouseholds,'b')

hold on;
plot(YearArr{bestRMSEIndex},NumHouseArr{bestRMSEIndex},'r');
        
legend('Target','LowestRMSE');
hold off;

% Histogram and target dist

% figure(4);
% subplot(2,1,1); 
% [n1,x1] = hist(rmseT, ceil(sqrt(runsize)));
% bar(x1,n1/(runsize*(x1(2)-x1(1))));   colormap summer;   hold on;   % Normalized histogram
% %plot(xx, p(xx)/trapz(xx,p(xx)), 'r-', 'LineWidth', 3);        % Normalized function
% grid on;   xlim([0 max(rmseT)]);   
% title('Distribution of samples', 'FontSize', 14);
% ylabel('Probability density function', 'FontSize', 12);