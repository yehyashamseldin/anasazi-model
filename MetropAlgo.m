
function [posterior_theta,theta_curr,rmseCurr] = MetropAlgo(theta_curr,theta_prop,rmseProp,rmseCurr,mu,sigma,posterior_theta,i)
    

    posterior_prop = normpdf(theta_prop,mu,sigma)*rmseProp;

    posterior_curr = normpdf(theta_curr,mu,sigma)*rmseCurr;

    p_accept_theta_prop = min(1,posterior_prop/posterior_curr);

    rand_unif = rand;

    if (p_accept_theta_prop>rand_unif)
        theta_curr= theta_prop;
        %else thete_curr = itself
        rmseCurr = rmseProp;
    end

    posterior_theta(i) = theta_curr;

   
    
    
    
end