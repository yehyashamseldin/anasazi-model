/*Cell.cpp*/
#include "repast_hpc/Point.h"
#include "CellClass.h"
#include <stdio.h>

Cell::Cell(){

}


Cell::Cell(int x ,int y){
    xCoor = x;
    yCoor= y;
    isWaterSource=0;
    zone=0;
    maizeYield=0;
    isField=0;
    isResidential=0;
}

Cell::~Cell(){

}

void Cell::setYield(int yield){
    maizeYield = yield;
}

void Cell::setWaterSource(int water){
    isWaterSource = water;
}

void Cell::setIsField(int field){
    isField = field;
}

void Cell::setIsRes(int res){
    isResidential = res;
}

int Cell::getIsField(){
    return isField;

}

int Cell::getIsRes(){
    return isResidential;

}

int Cell::getWaterSource(){
    return isWaterSource;

}

int Cell::getYield(){
    return maizeYield;

}

std::vector<int> Cell::getCoor(){
    std::vector<int> coor;
    coor.push_back(xCoor);
    coor.push_back(yCoor);
    return coor;
} 