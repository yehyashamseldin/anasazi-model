/*Network.cpp*/
#include "Network.h"
#include <stdio.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <boost/mpi.hpp>
#include "repast_hpc/RepastProcess.h"


#include "CellClass.h"

using namespace std;


MapNetwork::MapNetwork(){
    

    initializeCells();
    readClimateData();
    setCellClimateData(0);
}

MapNetwork::~MapNetwork(){
   
}

void MapNetwork::initializeCells(){
    for (int y = 0; y < 120; y++) {    

        std::vector<Cell*> c;
        for (int x = 0; x < 80; x++) {
            Cell* nextCell = new Cell(x,y);

            c.push_back(nextCell);           
        }
        cells.push_back(c);
    }
}

void MapNetwork::setCellClimateData(int year){

    
    for(int y = 0; y<120;y++  )
    {   
        
        for(int x = 0;x<80;x++)
        {
            
            cells[y][x]->setWaterSource(cellYieldWater[year][x][y][1]);
            if(cells[y][x]->getWaterSource() == 1)
            {
                cells[y][x]->setYield(0);
            }
            else
            {
                cells[y][x]->setYield(cellYieldWater[year][x][y][0]);
            }
            
        }
    }

}

void MapNetwork::readMapFile(){
    int x, y;
    std::string lineBuffer;

    std::ifstream csvMapFile("/home/samuel/ACS6132/Group/anasazi-model/data/map.csv"); // declare file stream
    
    if(csvMapFile.good()){
        while (getline(csvMapFile, lineBuffer))
        {
        
            if (lineBuffer.length() == 0){
                std::cerr <<"lineBuffer == 0 "<< std::endl;
            }
            if(lineBuffer[0]=='\"'){
            }
            else
            {
                std::stringstream ss(lineBuffer);
                string strX;
                string strY;
                string color;
                string zone;
                string maizeZone;
                int maizeZoneType;


                getline(ss, strX, ',');
                getline(ss, strY, ',');
                getline(ss, color, ',');
                getline(ss, zone, ','); 
                getline(ss, maizeZone, ',');
            
                if (zone == "\"Empty\""){//Empty,Natural,Kinbiko,Uplands,North,Mid,Dunes,General
                //"general","north","mid","natural","upland","kinbiko"
                    maizeZoneType = 0;
                }
                else if (zone == "\"Natural\"")
                {
                    maizeZoneType = 4;
                }
                else if (zone == "\"Kinbiko\"")
                {
                    maizeZoneType = 6;
                }
                else if (zone == "\"Uplands\"")
                {
                    maizeZoneType = 5;
                }
                else if (zone == "\"North\"")
                {
                    maizeZoneType = 2;
                }
                else if (zone == "\"Mid\"")
                {
                    maizeZoneType = 3;
                }
                else if (zone == "\"North Dunes\"" || zone == "\"Mid Dunes\"")
                {
                    maizeZoneType = 7;
                }
                else if (zone == "\"General\"")
                {
                    maizeZoneType = 1;
                }
                else
                {
                    maizeZoneType = 8;
                }
                x = repast::strToInt(strX);
                y = repast::strToInt(strY);
                

                zoneAreaArray[x][y] = maizeZoneType;
              
            }
        }
        
        csvMapFile.close();
    }
    else
    {
        std::cerr<<"Map CSV data not good - probably have wrong file address " << std::endl;
    }
    
    

}

void MapNetwork::readPDSIFile(){
    std::string lineBuffer;
    std::ifstream csvPDSIFile("/home/samuel/ACS6132/Group/anasazi-model/data/pdsi.csv"); // declare file stream

    if(csvPDSIFile.good()){

        while (getline(csvPDSIFile, lineBuffer))
        {
        
            if (lineBuffer.length() == 0){
                std::cerr <<"lineBuffer == 0 "<< std::endl;
            }
            if(lineBuffer[0]=='\"'){
            }
            else
            {
            
            
                
                std::stringstream ss(lineBuffer);
                string wordBuffer;
                int year;
                int general;
                int north;
                int mid;
                int natural;
                int upland;
                int kinbiko;
                //int PDSIZoneArray[550][7];

                getline(ss, wordBuffer, ',');
                year = repast::strToInt(wordBuffer);
                PDSIZoneArray[year-800][0] = year;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                if(general <-3){
                    PDSIZoneArray[year-800][1] = 514;
                }else if (general >=-3 && general <-1)
                {
                    PDSIZoneArray[year-800][1] = 599;
                }else if (general >=-1 && general <1)
                {
                    PDSIZoneArray[year-800][1] = 684;
                }else if (general >=1 && general <3)
                {
                    PDSIZoneArray[year-800][1] = 824;
                }else if (general >=3)
                {
                    PDSIZoneArray[year-800][1] = 961;
                }
                

                getline(ss, wordBuffer, ',');
                north = repast::strToInt(wordBuffer);
                if(north <-3){
                    PDSIZoneArray[year-800][2] = 617;
                }else if (north >=-3 && north <-1)
                {
                    PDSIZoneArray[year-800][2] = 719;
                }else if (north >=-1 && north <1)
                {
                    PDSIZoneArray[year-800][2] = 821;
                }else if (north >=1 && north <3)
                {
                    PDSIZoneArray[year-800][2] = 988;
                }else if (north >=3)
                {
                    PDSIZoneArray[year-800][2] = 1153;
                }

                getline(ss, wordBuffer, ',');
                mid = repast::strToInt(wordBuffer);

                if(mid <-3){
                    PDSIZoneArray[year-800][3] = 617;
                }else if (mid >=-3 && mid <-1)
                {
                    PDSIZoneArray[year-800][3] = 719;
                }else if (mid >=-1 && mid <1)
                {
                    PDSIZoneArray[year-800][3] = 821;
                }else if (mid >=1 && mid <3)
                {
                    PDSIZoneArray[year-800][3] = 988;
                }else if (mid >=3)
                {
                    PDSIZoneArray[year-800][3] = 1153;
                }


                getline(ss, wordBuffer, ','); 
                natural = repast::strToInt(wordBuffer);
                if(natural <-3){
                    PDSIZoneArray[year-800][4] = 617;
                }else if (natural >=-3 && natural <-1)
                {
                    PDSIZoneArray[year-800][4] = 719;
                }else if (natural >=-1 && natural <1)
                {
                    PDSIZoneArray[year-800][4] = 821;
                }else if (natural >=1 && natural <3)
                {
                    PDSIZoneArray[year-800][4] = 988;
                }else if (natural >=3)
                {
                    PDSIZoneArray[year-800][4] = 1153;
                }

                getline(ss, wordBuffer, ',');
                upland = repast::strToInt(wordBuffer);
                if(upland <-3){
                    PDSIZoneArray[year-800][5] = 617;
                }else if (upland >=-3 && upland <-1)
                {
                    PDSIZoneArray[year-800][5] = 719;
                }else if (upland >=-1 && upland <1)
                {
                    PDSIZoneArray[year-800][5] = 821;
                }else if (upland >=1 && upland <3)
                {
                    PDSIZoneArray[year-800][5] = 988;
                }else if (upland >=3)
                {
                    PDSIZoneArray[year-800][5] = 1153;
                }

                getline(ss, wordBuffer, ',');
                kinbiko = repast::strToInt(wordBuffer);
                if(kinbiko <-3){
                    PDSIZoneArray[year-800][6] = 617;
                }else if (kinbiko >=-3 && kinbiko <-1)
                {
                    PDSIZoneArray[year-800][6] = 719;
                }else if (kinbiko >=-1 && kinbiko <1)
                {
                    PDSIZoneArray[year-800][6] = 821;
                }else if (kinbiko >=1 && kinbiko <3)
                {
                    PDSIZoneArray[year-800][6] = 988;
                }else if (kinbiko >=3)
                {
                    PDSIZoneArray[year-800][6] = 1153;
                }  
                
            }
        }
        csvPDSIFile.close();
    }
    else
    {
        std::cerr<<"PDSI CSV data not good" << std::endl;
    }
}

void MapNetwork::readWaterFiles(){

    std::string lineBuffer;
    std::ifstream csvHydroFile("/home/samuel/ACS6132/Group/anasazi-model/data/hydro.csv"); // declare file stream

    if(csvHydroFile.good()){

    
        while (getline(csvHydroFile, lineBuffer))
        {
        
            if (lineBuffer.length() == 0){
                std::cerr <<"lineBuffer == 0 "<< std::endl;
            }
            if(lineBuffer[0]=='\"'){
            }
            else
            {
                
                std::stringstream ss(lineBuffer);
                string wordBuffer;
                int year;
                int general;
                int north;
                int mid;
                int natural;
                int upland;
                int kinbiko;
                

                getline(ss, wordBuffer, ',');
                year = repast::strToInt(wordBuffer);
                hydroArray[year-800][0] = year;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                hydroArray[year-800][1] = general;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                hydroArray[year-800][2] = north;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                hydroArray[year-800][3] = mid;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                hydroArray[year-800][4] = natural;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                hydroArray[year-800][5] = upland;

                getline(ss, wordBuffer, ',');
                general = repast::strToInt(wordBuffer);
                hydroArray[year-800][6] = kinbiko;

                
            }
        }
        csvHydroFile.close();
    }
    else
    {
        std::cerr<<"hydro CSV data not good" << std::endl;
    }
    

    std::ifstream csvWaterFile("/home/samuel/ACS6132/Group/anasazi-model/data/water.csv"); // declare file stream
    for(int i =0;i<80;i++){
        for(int j =0;j<120;j++){
            waterTypeArray[i][j][0] = 0;
            waterTypeArray[i][j][1] = 0;
            waterTypeArray[i][j][2] = 0;
        }
    }
    
    if(csvWaterFile.good()){

    
        while ( getline(csvWaterFile, lineBuffer))
        {
        
            if (lineBuffer.length() == 0){
                std::cerr <<"lineBuffer == 0 "<< std::endl;
            }
            if(lineBuffer[0]=='\"'){
            }
            else
            {
                std::stringstream ss(lineBuffer);
                string wordBuffer;
                int type;
                int startDate;
                int endDate;
                int x;
                int y;

                
               

                getline(ss, wordBuffer, ',');
                getline(ss, wordBuffer, ',');
                getline(ss, wordBuffer, ',');
                getline(ss, wordBuffer, ',');
                type = repast::strToInt(wordBuffer);

                getline(ss, wordBuffer, ',');
                startDate = repast::strToInt(wordBuffer);

                getline(ss, wordBuffer, ',');
                endDate = repast::strToInt(wordBuffer);

                getline(ss, wordBuffer, ',');
                x = repast::strToInt(wordBuffer);

                getline(ss, wordBuffer, ',');
                y = repast::strToInt(wordBuffer);
                //std::cerr<<"type "<<type<<std::endl;
                waterTypeArray[x][y][0] = type;
                waterTypeArray[x][y][1] = startDate;
                waterTypeArray[x][y][2] = endDate;

                
            }
        }
        
        csvWaterFile.close();
    }
    else
    {
        std::cerr<<"Water CSV data not good" << std::endl;
    }
}

void MapNetwork::readClimateData(){
    
    
    readMapFile();
    
    readPDSIFile();
    
    readWaterFiles();
    std::cerr<<waterTypeArray[49][93][0]<<std::endl;
    int year,x,y;
    //cellYieldWater[550][80][120][2]; -> [Year], [x],[y], [Yield isWater] 
    //zoneAreaArray[80][120]; -> width and height of network
    //PDSIZoneArray[550][7]; -> "year","general","north","mid","natural","upland","kinbiko"
    //waterTypeArray[80][120][3]; -> "x","y","type","start date","end date"
    //hydroArray[550][7]; -> "year","general","north","mid","natural","upland","kinbiko"

    
    for(year = 0;year<550;year++)
    {
        for(x = 0;x<80;x++)
        {
            for(y = 0;y<120;y++)
            {
                cellYieldWater[year][x][y][1] = 0;

                if(zoneAreaArray[x][y]==0)
                {
                    cellYieldWater[year][x][y][0] = -1;
                }
                else if (zoneAreaArray[x][y]==7)//dunes
                {
                    cellYieldWater[year][x][y][0] = -1;
                } 
                else
                {   
                    //set yearly yield quantity
                    cellYieldWater[year][x][y][0] = PDSIZoneArray[year][zoneAreaArray[x][y]];
                    
                    //std::cout<<"type "<<waterTypeArray[x][y][0]<<std::endl;
                    if(waterTypeArray[x][y][0]==2){
                        //std::cerr<<"always water "<<x<<","<<y<<std::endl;
                        cellYieldWater[year][x][y][1] = 1;
                    }else if (waterTypeArray[x][y][0]==3)
                    {
                        //std::cerr<<"year dependant water "<<x<<","<<y<<std::endl;
                        if(year>=waterTypeArray[x][y][1] && year <= waterTypeArray[x][y][2]){
                            cellYieldWater[year][x][y][1] = 1;
                        }
                    }else if (waterTypeArray[x][y][0]==1)
                    {
                        //std::cerr<<"zone dependant water "<<x<<","<<y<<std::endl;
                        if(hydroArray[year][zoneAreaArray[x][y]]>0){
                            cellYieldWater[year][x][y][1] = 1;
                        }
                    }
                    else
                    {
                        //std::cerr<<"broken water"<<x<<","<<y<<std::endl;
                    }
                    
                }

                
            }
        }   
    }
}

int MapNetwork::getCellZoneType(int x,int y){
    return zoneAreaArray[x][y];
    
}
