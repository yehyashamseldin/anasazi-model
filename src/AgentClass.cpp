#include "AgentClass.h"
#include "repast_hpc/Moore2DGridQuery.h"
#include "repast_hpc/VN2DGridQuery.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"
#include <stdio.h>
#include "CellClass.h"
#include <random>
#include <cmath>


Agent::Agent(repast::AgentId id): agentId(id) {
	resLocCell = new Cell();
	fieldLocCell = new Cell();
	maizeQuantity = 0;
	currentAge = 0;
	fertileAge = 0;
	infertileAge = 0;
	isFertile = false;
	isExpired = false;
	fissions = false;
	maxStock = 0;
	maizePrediction = 0;
	percentHarvestEfficiency = 0;
	numOfIndividuals = 0;
	fissionProbability = 0;
	individualConsumptionRate = 0;
	stockNeeded = 0;
	maxDistanceResAndField = 0;


}

Agent::~Agent() {
	//delete resLocCell;
	//delete fieldLocCell;
}


void Agent::updateAgentProperties(std::vector<double> agentProps){
	deathAge = agentProps[0];
	fertileAge = agentProps[1];
	infertileAge = agentProps[2];
	maxStock = agentProps[3];
	percentHarvestEfficiency = agentProps[4];
	individualConsumptionRate = agentProps[5];
	fissionProbability = agentProps[6];
	maxDistanceResAndField= agentProps[7];
	numOfIndividuals = agentProps[7];
	stockNeeded = numOfIndividuals * individualConsumptionRate;
	//std::cerr<<"Death Age = " <<deathAge<<std::endl;
	//std::cerr<<"Fertile Age = " <<fertileAge<<std::endl;
	//std::cerr<<"Fission probability = " <<fissionProbability<<std::endl;
}

void Agent::updateStatus(repast::SharedContext<Agent>* context,repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* space, MapNetwork* map){
	
	//debugTestCell(fieldLocCell);
	fissions = false;	
	currentAge ++;
	int ranNum;
	int probFission;
	probFission = (int)(fissionProbability*1000);
	if(currentAge>=deathAge)
	{
		isExpired = true;
	}
	else
	{
		maizeQuantity += (fieldLocCell->getYield())*percentHarvestEfficiency;
		if(maizeQuantity>maxStock)
		{
			maizeQuantity = maxStock;
		}
		//std::cerr<<"Maize quantity = "<<maizeQuantity<<std::endl;
		if(currentAge>=fertileAge && currentAge<infertileAge)
		{
			//std::cerr<<"agent is fertile -> current age = " << currentAge<<std::endl;
			isFertile = true;
			repast::IntUniformGenerator genNum = repast::Random::instance()->createUniIntGenerator(0, 1000);
			ranNum = genNum.next();
			//std::cerr<<"ranNum = "<<ranNum<<" Fission prob = "<<probFission<<std::endl;
			if (ranNum <= probFission){
				//std::cerr<<"fission = true"<<std::endl;
				fissions = true;
			}
			
		}
		decideMoveLocation(space,map);
		maizeQuantity -= individualConsumptionRate*numOfIndividuals;
		if (maizeQuantity<0){
			maizeQuantity =0;
		}
	}

}	


void Agent::setInitialCellRes(Cell* cell){
	resLocCell = cell;
	resLocCell->setIsRes(1);
	
	
}

Cell* Agent::getCellRes(){
	return resLocCell;
}

void Agent::debugTestCell(Cell* cellTest){
	std::cout<< cellTest->getIsRes()<<" getIsRes"<<std::endl;
	std::cout<< cellTest->getIsField()<<" getIsField"<<std::endl;
	std::cout<< cellTest->getWaterSource()<<" getWaterSource"<<std::endl;
	std::cout<< cellTest->getYield()<<" getYield"<<std::endl;
	std::cout<< (cellTest->getCoor())[0]<<" = x "<<std::endl;
	std::cout<< (cellTest->getCoor())[1]<<" = y "<<std::endl;
}


void Agent::decideMoveLocation(repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent> >* space, MapNetwork* map){
	

	//decide cell range to check - 1km away from centre = 10 cells from centre
	std::vector<int> coorRes;
	coorRes = resLocCell->getCoor();
	//std::cerr << "coorRes = ResLocCell x = " << coorRes[0] << " y = "<<coorRes[1]<< std::endl;
	int xStart = 0;//coorRes[0] - 10;
	int yStart = 0;//coorRes[0] - 10;

	int xEnd = 80;//coorRes[0] + 10;
	int yEnd = 120;//coorRes[0] + 10;
	bool isSatisfied = false;
	double preDistance=1000000;
	double curDistance=0;
	Cell* closestFieldCell;
	//std::cerr<<"Maize quantity = "<<maizeQuantity<<std::endl;

	if((maizeQuantity+ fieldLocCell->getYield())< stockNeeded){ 	//the stock maize in current year and field occupied by the agent doesn't match the stock needed for the year ahead
		fieldLocCell->setIsField(0);
		//std::cerr<<"FieldLoop"<<std::endl;
		//searching for new field location
		for(int y = yStart; y < yEnd; y++){ 		
			for (int x = xStart; x < xEnd; x++){
				if((x >= 0 && x<80) && (y >= 0 && y<120))//check its within grid
				{
					Cell* tempCell = map->cells[y][x];
					
					//std::cerr<<"curDistance "<<curDistance<<" "<<y<<" - "<<coorRes.at(1)<<": "<<x<<" - "<<coorRes.at(0)<<std::endl;
					if(tempCell->getIsField() == 0 && tempCell->getWaterSource() == 0 && tempCell->getIsRes() == 0 && tempCell->getYield() >= 800){ 
										//if the cell is not occupied - positive outcome
						curDistance = std::sqrt(std::pow(y-coorRes.at(1),2) + std::pow(x-coorRes.at(0),2));
						isSatisfied = true;
						if(curDistance <preDistance)
						{
							closestFieldCell = tempCell;
							preDistance = curDistance;
							
						}
						
					}
				}
			}
		}
		
		//std::cerr<<"Field location moved"<<std::endl;
		

		//"agent leaves the valley"
		if(isSatisfied == false){				//"agent leaves the valley", assuming termination
			//std::cout<<"Agent has left the valley -> "<<agentId<<std::endl;	
			
			isExpired = true;	
			fieldLocCell->setIsField(0);
			resLocCell->setIsRes(0);					//escape while loop due to death
		}
		else
		{
			//std::cerr<<"Distance to new field = "<< preDistance<<std::endl;
			fieldLocCell = closestFieldCell;
			fieldLocCell->setIsField(1);
			isSatisfied = false;
			if (preDistance>10)
			{
				
				resLocCell->setIsRes(0);
				//searching for new residence SEARCH 2
				//look through the cells within 1 km of the newly found field?
				std::vector<int> coorField;
				coorField = fieldLocCell->getCoor();
				int xStart = coorField[0] - 10;
				int yStart = coorField[0] - 10;

				int xEnd = coorField[0] + 10;
				int yEnd = coorField[0] + 10;
				//std::cerr<<"Res loop"<<std::endl;
				for(int y = yStart; y < yEnd; y++){ 		//loop through all nearby cells
					for (int x = xStart; x < xEnd; x++){
						if((x >= 0 && x<80) && (y >= 0 && y<120))//check its within grid
						{
							
							
							//std::cerr<<"X = "<<x<<" Y = " <<y<<std::endl;
							if(std::sqrt(std::pow(y-coorRes.at(1),2) + std::pow(x-coorRes.at(0),2)) <maxDistanceResAndField)
							{
								Cell* tempCell = map->cells[y][x];
								if(isSatisfied !=true)
								{
									if(tempCell->getIsField() == 0 & tempCell->getWaterSource() == 0){ 				//if the cell is not occupied - positive outcome
													//if the cell has the minimum maize required to survive - positive outcome
										resLocCell = tempCell;
										resLocCell->setIsRes(1);
										isSatisfied = true;
										space->moveTo(agentId,tempCell->getCoor()); 
										//std::cerr<<"Agent residence moved"<<std::endl;
										
									}		
								}
								
								
							}
						}
					}
				}
			}
			else
			{
				isSatisfied =true;
			}
			
		
			if(isSatisfied == false){				//"agent leaves the valley", assuming termination
				isExpired = true;	
				fieldLocCell->setIsField(0);
				resLocCell->setIsRes(0);	
				//std::cout<<"Agent has left the valley -> "<<agentId<<std::endl;					//escape while loop due to death
			}
		}
		

		
		
	}
		
}

bool Agent::getIsExpired(){
	return isExpired;
}

bool Agent::getFissions(){
	return fissions;
}