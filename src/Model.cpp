/* Model.cpp */

#include <stdio.h>
#include <vector>
#include <boost/mpi.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/RepastProcess.h"
#include "repast_hpc/Utilities.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/initialize_random.h"
#include "repast_hpc/SVDataSetBuilder.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"
#include <boost/mpi/communicator.hpp>
#include "Model.h"
#include "Network.h"
#include "CellClass.h"
#include "AgentClass.h"
#include <sstream>
#include <iostream>
#include <fstream>

AnasaziModel::AnasaziModel(std::string propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm){
	props = new repast::Properties(propsFile, argc, argv, comm);
	endYear = repast::strToInt(props->getProperty("end.year"));
	stopAt =  repast::strToInt(props->getProperty("stop.at"));
	countOfAgents = repast::strToInt(props->getProperty("count.of.agents"));
	AgentCountId = countOfAgents;
	boardWidth = repast::strToInt(props->getProperty("board.width"));
	boardHeight = repast::strToInt(props->getProperty("board.height"));
	testing = false;
	initializeRandom(*props, comm);
	currentYear = 0;

	repast::Point<double> origin(0,0); 
	repast::Point<double> extent(boardWidth,boardHeight);
	repast::GridDimensions gd(origin, extent);
	std::vector<int> processDims;
	processDims.push_back(1);
	processDims.push_back(1);
	numOfAgentsEachYear = new std::vector<int>;

	map = new MapNetwork;
	
	discreteSpace = new repast::SharedDiscreteSpace<Agent, repast::StrictBorders, repast::SimpleAdder<Agent>>("AgentDiscreteSpace", gd, processDims, 0, comm);
	
	context.addProjection(discreteSpace);
}

AnasaziModel::~AnasaziModel(){
	delete props;
	delete map;
}

void AnasaziModel::initAgents(){
	//std::cerr<<"Initiating agents - initAgents()"<<std::endl;

	int rank = repast::RepastProcess::instance()->rank(); 	
	repast::IntUniformGenerator genX = repast::Random::instance()->createUniIntGenerator(1, boardWidth-1); //random from 1 to boardSize 
	repast::IntUniformGenerator genY = repast::Random::instance()->createUniIntGenerator(1, boardHeight-1); //random from 1 to boardSize 
	
	std::vector<double> agentProps;

	double deathMax = repast::strToDouble(props->getProperty("death.age.max"));
	double deathMin = repast::strToDouble(props->getProperty("death.age.min"));
	double deathMean = (deathMax+deathMin)/2;
	double deathDeviation = (deathMean-deathMin)/4;

	double fertMax = repast::strToDouble(props->getProperty("fertile.age.max"));
	double fertMin = repast::strToDouble(props->getProperty("fertile.age.min"));
	double fertMean = (fertMax+fertMin)/2;
	double fertDeviation = (fertMean-fertMin)/4;

	double fissMax = repast::strToDouble(props->getProperty("fission.prob.max"));
	double fissMin = repast::strToDouble(props->getProperty("fission.prob.min"));
	double fissMean = (fissMax+fissMin)/2;
	double fissDeviation = (fissMean-fissMin)/4;
	
	double harvMax = repast::strToDouble(props->getProperty("percent.harvest.efficiency.max"));
	double harvMin = repast::strToDouble(props->getProperty("percent.harvest.efficiency.min"));
	double harvMean = (harvMax+harvMin)/2;
	double harvDeviation = (harvMean-harvMin)/4;
	
	double indivMax = repast::strToDouble(props->getProperty("individual.consumption.rate.max"));
	double indivMin = repast::strToDouble(props->getProperty("individual.consumption.rate.min"));
	double indivMean = (indivMax+indivMin)/2;
	double indivDeviation = (indivMean-indivMin)/4;
	
	repast::NormalGenerator genIndivProb = repast::Random::instance()->createNormalGenerator(indivMean,indivDeviation); 


	repast::NormalGenerator genHarvProb = repast::Random::instance()->createNormalGenerator(harvMean, harvDeviation); 

	repast::NormalGenerator genFissProb = repast::Random::instance()->createNormalGenerator(fissMean,fissDeviation); 
	
	repast::NormalGenerator genFertAge = repast::Random::instance()->createNormalGenerator(fertMean,fertDeviation); 

	repast::NormalGenerator genDeathAge = repast::Random::instance()->createNormalGenerator(deathMean,deathDeviation); 

	agentProps.push_back(repast::strToInt(props->getProperty("death.age"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("fertile.age"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("infertile.age"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("max.stock"))); 
	agentProps.push_back(repast::strToDouble(props->getProperty("percent.harvest.efficiency"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("individual.consumption.rate"))); 
	agentProps.push_back(repast::strToDouble(props->getProperty("fission.probabilty"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("max.distance.res.field"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("num.of.individuals"))); 
	
	for (int i = 0; i < countOfAgents; i++){ 
		int xRand, yRand;
		do{
			xRand = genX.next();
			yRand = genY.next();
		} while (map->getCellZoneType(xRand,yRand) == 0);

	
		agentProps[0] = genDeathAge.next();
		agentProps[4] = genHarvProb.next();
		agentProps[5] = genIndivProb.next();
		agentProps[6] = genFissProb.next();
		//agentProps[6] = 0.125;
		do{
			agentProps[1] = genFertAge.next();
		} while (agentProps[1] > agentProps[0]);	

		repast::Point<int> initialLocation(xRand, yRand);
		repast::AgentId id(i, rank, 0);
		id.currentRank(rank);

		Agent* agent = new Agent(id);
		agent->updateAgentProperties(agentProps);

		
		
		agent->setInitialCellRes(getCell(xRand,yRand));
		

		//std::cerr<<" coor = "<<agent->getCellRes()->getCoor().at(0)<<","<<agent->getCellRes()->getCoor().at(1)<<std::endl;
		context.addAgent(agent);
		discreteSpace->moveTo(id, initialLocation);
		agent->decideMoveLocation(discreteSpace,map);
	}
	
}

void AnasaziModel:: doPerTick(){
	
	//printToScreen();
	std::cout<<"Year = "<<currentYear<<" Number of households = "<<countOfAgents<<std::endl;
	
	map->setCellClimateData(currentYear);
	int temp;
	if (countOfAgents>0)
	{

		std::vector<Agent*> agents;
		context.selectAgents(repast::SharedContext<Agent>::LOCAL, countOfAgents, agents);
		temp = countOfAgents;
		for(int i = 0;i<temp;i++)
		{	
			
			agents[i]->updateStatus(&context, discreteSpace, map);
			if(agents[i]->getIsExpired() == true){
				//std::cerr<<agents[i]->getId();
				context.removeAgent(agents[i]->getId());
				//std::cerr<<" -> agent removed"<<std::endl;
				countOfAgents--;
				
			}
			else if(agents[i]->getFissions() == true)
			{
				//std::cerr<<"create new agents"<<std::endl;
				
				newAgent();
			}
		} 
		
		if (countOfAgents!=0)
		{
			context.selectAgents(repast::SharedContext<Agent>::LOCAL, countOfAgents, agents);
			
			double currentTick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
				
			if (currentTick==1){ //print at the beginning and the end of the simulation 
				printToScreen();
				/*it = agents.begin();
				while(it != agents.end()){
					(*it)->madebugTest();
					it++;
				}*/
			}
			
		}
		else
		{
			//std::cerr<<"All agents removed"<<std::endl;
		}
	}
	
	numOfAgentsEachYear->push_back(countOfAgents);
	if(countOfAgents >= 300)
	{
		countOfAgents = -1;
		stopAt = currentYear+1;
		std::cerr<<"StopAt = "<<stopAt<<std::endl;
	}
	currentYear++;
	//std::cerr<<map->cells[96][47]->getWaterSource()<<std::endl;
	
}
void AnasaziModel::newAgent(){
	repast::IntUniformGenerator genX = repast::Random::instance()->createUniIntGenerator(1, boardWidth-1); //random from 1 to boardSize 
	repast::IntUniformGenerator genY = repast::Random::instance()->createUniIntGenerator(1, boardHeight-1); //random from 1 to boardSize 
	
	std::vector<double> agentProps;

	double deathMax = repast::strToDouble(props->getProperty("death.age.max"));
	double deathMin = repast::strToDouble(props->getProperty("death.age.min"));
	double deathMean = (deathMax+deathMin)/2;
	double deathDeviation = (deathMean-deathMin)/4;

	double fertMax = repast::strToDouble(props->getProperty("fertile.age.max"));
	double fertMin = repast::strToDouble(props->getProperty("fertile.age.min"));
	double fertMean = (fertMax+fertMin)/2;
	double fertDeviation = (fertMean-fertMin)/4;

	double fissMax = repast::strToDouble(props->getProperty("fission.prob.max"));
	double fissMin = repast::strToDouble(props->getProperty("fission.prob.min"));
	double fissMean = (fissMax+fissMin)/2;
	double fissDeviation = (fissMean-fissMin)/4;

	double harvMax = repast::strToDouble(props->getProperty("percent.harvest.efficiency.max"));
	double harvMin = repast::strToDouble(props->getProperty("percent.harvest.efficiency.min"));
	double harvMean = (harvMax+harvMin)/2;
	double harvDeviation = (harvMean-harvMin)/4;
	
	double indivMax = repast::strToDouble(props->getProperty("individual.consumption.rate.max"));
	double indivMin = repast::strToDouble(props->getProperty("individual.consumption.rate.min"));
	double indivMean = (indivMax+indivMin)/2;
	double indivDeviation = (indivMean-indivMin)/4;
	
	repast::NormalGenerator genIndivProb = repast::Random::instance()->createNormalGenerator(indivMean,indivDeviation); 

	repast::NormalGenerator genHarvProb = repast::Random::instance()->createNormalGenerator(harvMean,harvDeviation); 

	repast::NormalGenerator genFissProb = repast::Random::instance()->createNormalGenerator(fissMean,fissDeviation); 

	repast::NormalGenerator genFertAge = repast::Random::instance()->createNormalGenerator(fertMean,fertDeviation); 

	repast::NormalGenerator genDeathAge = repast::Random::instance()->createNormalGenerator(deathMean,deathDeviation); 
	
	agentProps.push_back(repast::strToInt(props->getProperty("death.age"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("fertile.age"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("infertile.age"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("max.stock"))); 
	agentProps.push_back(repast::strToDouble(props->getProperty("percent.harvest.efficiency"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("individual.consumption.rate"))); 
	agentProps.push_back(repast::strToDouble(props->getProperty("fission.probabilty"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("max.distance.res.field"))); 
	agentProps.push_back(repast::strToInt(props->getProperty("num.of.individuals")));

	int xRand, yRand;

	do{
		xRand = genX.next();
		yRand = genY.next();
	} while (map->getCellZoneType(xRand,yRand) == 0);

	int rank = repast::RepastProcess::instance()->rank(); 
	AgentCountId++;
	repast::AgentId id(AgentCountId, rank, 0);
	id.currentRank(rank);
	Agent* agent = new Agent(id);

	agentProps[0] = genDeathAge.next();
	agentProps[4] = genHarvProb.next();
	agentProps[5] = genIndivProb.next();
	agentProps[6] = genFissProb.next();
	//agentProps[6] = 0.125;

	do{
		agentProps[1] = genFertAge.next();
	} while (agentProps[1] > agentProps[0]);


	agent->updateAgentProperties(agentProps);
	

	agent->setInitialCellRes(getCell(xRand,yRand));
	
	context.addAgent(agent);
	countOfAgents++;

	repast::Point<int> location(xRand, yRand);
	//std::cerr<<"New Agent location -> "<<location<<std::endl;
	discreteSpace->moveTo(id, location);
	agent->decideMoveLocation(discreteSpace,map);
	//std::cerr<<"New Agent created -> "<<agent->getId()<<std::endl;
	if(testing == true){
		std::cout<<agent->getId()<<":\ndeath age = "<<agent->getDeathAge()<<"\nfertile age = "<<agent->getFertileAge()<<std::endl;
		std::cout<<"Located at ("<<xRand<<","<<yRand<<")"<<std::endl;
	}
	
}


void AnasaziModel::initSchedule(repast::ScheduleRunner& runner){
	//runner.scheduleEvent(1.5, repast::Schedule::FunctorPtr(new repast::MethodFunctor<AnasaziModel> (this, &AnasaziModel::UnitTests)));
	runner.scheduleEvent(1, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<AnasaziModel> (this, &AnasaziModel::doPerTick)));
	//runner.scheduleEvent(50, 50, repast::Schedule::FunctorPtr(new repast::MethodFunctor<AnasaziModel> (this, &AnasaziModel::printToScreen)));
	
	runner.scheduleEndEvent(repast::Schedule::FunctorPtr(new repast::MethodFunctor<AnasaziModel> (this, &AnasaziModel::recordResults)));
	runner.scheduleStop(stopAt);
}

void AnasaziModel::printToScreen() {
	//print board to screen 
	int z;
	
	std::cout << boardWidth << ", " << boardHeight << std::endl; 

	std::vector<Agent*> agentList;
	for (int y=-1; y<=boardHeight; y++) {
		for (int x=-1; x<=boardWidth; x++) {
			if (y==-1 || y==boardHeight) 
				std::cout << " ";// "-"
			else if (x==-1 || x==boardWidth) 
				std::cout << " ";// "|"
			else { 
				
				
				
				agentList.clear();
				discreteSpace->getObjectsAt(repast::Point<int>(x, y), agentList);

				z = map->getCellZoneType(x,y);
				if(map->cells[y][x]->getWaterSource()==1)
				{
					std::cout<< "💦";
				}
				else if(map->cells[y][x]->getIsRes() ==1)
				{
					std::cout << "A";
				}
				else if(map->cells[y][x]->getIsField() ==1)
				{
					std::cout << "F";
				}
				else
				{
					switch(z){
					case 0: std::cout<< " ";
						break;
					case 1:	std::cout<< "☐";
						break;
					case 2: std::cout<< "❄";
						break;	
					case 3:	std::cout<< "▨";
						break;
					case 4: std::cout<< "⚘";
						break;
					case 5:	std::cout<< "⛰";
						break;
					case 6: std::cout<< "◯";
						break;
					case 7:	std::cout<< "▲";//u25b0
						break;
					case 8:	std::cout<<"ERROR";
						break;
					default: std::cout<<"Default"; 
						break;
					}
				}
					
				

			} 
		} 
		std::cout << std::endl; 
		
	} 
	
}        
        

Cell* AnasaziModel::getCell(int x, int y){
	return map->cells[y][x];
}

void AnasaziModel::recordResults(){
	//printToScreen();
	std::ofstream myfile;
	myfile.open ("results.csv");
	myfile << "year, NumOfHouseholds\n";
	for(int i = 0;i<numOfAgentsEachYear->size();i++){
		myfile<<i<<","<<(*numOfAgentsEachYear)[i]<<"\n";
	}
	myfile.close();
}

void AnasaziModel::UnitTests()
{
	testing = true;
	std::cout<<"\n\n\nTest  1: instantiate an agent and update its properties\n"<<std::endl;
	newAgent();

	std::cout<<"\n\n\nTest  2: instantiate the network and update its properties\n"<<std::endl;	
	
	std::cout<<"Network created and filled with cells"<<std::endl;	

	std::cout<<"\n\n\nTest  3: Read archaelogical data as input and propagate to network\n"<<std::endl;
	printToScreen();
	std::cout<<"\n\n\nTest  4: Reduce yield to see effect on population\n"<<std::endl;
	std::cout<<"The yield of the cells change over time causing the agents to move around or leave the valley"<<std::endl;	

	std::cout<<"\n\n\nTest  5: Read death age\n"<<std::endl;
	std::vector<Agent*> agents;
	context.selectAgents(repast::SharedContext<Agent>::LOCAL, countOfAgents, agents);
	std::cout<<"Death age = "<<agents[0]->getDeathAge()<<std::endl;
	std::cout<<"\n\n\nTest  6: Generate an output file\n"<<std::endl;
	std::cout<<"Output file called results.txt generated at simulation completion\n"<<std::endl;
	testing = false;
}