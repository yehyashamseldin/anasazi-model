
setenv('LD_LIBRARY_PATH','//home/samuel/sfw/Boost/Boost_1.61/lib/://home/samuel/sfw/repast_hpc-2.3.0/lib/');

x1 = 23.9168;
x2 = 12.4478;

DeathAgeMsg = strcat(' death.age=',num2str(x1));
FertillityAgeMsg = strcat(' fertile.age=',num2str(x2));
disp(DeathAgeMsg);
disp(FertillityAgeMsg);
command = 'mpirun -n 1 bin/main.exe props/config.props props/model.props >output.txt';
command = strcat(command,DeathAgeMsg);
command = strcat(command,FertillityAgeMsg);
system(command);


ModelData = readtable('results.csv');
Myear = ModelData.year;    
MNumOfHouseholds = ModelData.NumOfHouseholds;

TargetData = readtable('CalibrationTargetData.csv');
Tyear = TargetData.Var1 - 800;    
TNumOfHouseholds = TargetData.Var2;

plot(Tyear,TNumOfHouseholds,Myear,MNumOfHouseholds);
